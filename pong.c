#include "raylib.h"

typedef enum GameScreen {LOGO = 0, TITLE, GAMEPLAY, ENDING} GameScreen;

//void ResetBall();

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen currentScreen = LOGO;  // 0=LOGO, 1=TITLE, 2=GAMEPLAY, 3=ENDING

    InitWindow(screenWidth, screenHeight, "raylib game - the final pong");
    
    int framesCounter = 0; //frame counter

    //--------------------JUGADORES Y BOLA-----------------------
    Rectangle recIzquierda;
    recIzquierda.x = 25;
    recIzquierda.y = screenHeight/2 - 50;
    recIzquierda.width = 30;
    recIzquierda.height = 100;

    Rectangle recDerecha;
    recDerecha.x = 750;
    recDerecha.y = screenHeight/2 - 50;
    recDerecha.width = 30;
    recDerecha.height = 100;
 
    Color colplayer = PINK;
    Color colenemy = SKYBLUE;
   
    int aceleracionY = 1;
    int aceleracionX = 1;

    float radio = 20.0;


    //ResetBall();
    Vector2 circle = { screenWidth/2, screenHeight/2 };
    Vector2 circleSpeed = { -GetRandomValue(5, 10), GetRandomValue(5, 10) };
    
    bool pause = false;
    //--------------------JUGADORES Y BOLA-----------------------
    
    
    //-------------------------MAQUINA---------------------------
    Rectangle recIa;
    recIa.x = screenWidth/2;
    recIa.y = 0;
    recIa.width = 2;
    recIa.height = screenHeight;
    int speedEnemy = 5;
    //-------------------------MAQUINA---------------------------
    
    
    //--------------------------LOGO-----------------------------
    bool fading = true;
    float alpha = 0.0f;
    float fadeSpeed = 0.02f;

    Texture2D texture = LoadTexture("gato.png");
    //--------------------------LOGO-----------------------------
    
    
    //--------------------------TITULO---------------------------
    const char title[15] = "THE FINAL PONG";
    const char start[15] = "PRESS ENTER";
    const char name[20] = "NIL.AYEON STUDIOS©";
    int nameSize = 20;
    Color nameColor = GRAY;
    int titleSize = 75;
    Color titleColor = SKYBLUE;
    int startSize = 50;
    Color startColor = GRAY;
    bool drawStart = false;
    int countFrames = 0;
    //--------------------------TITULO---------------------------
    
    
    //---------------------BARRA DE VIDA-------------------------
    int playerLife = 100;
    int enemyLife = 200;
    //---------------------BARRA DE VIDA-------------------------
    
    
    int secondsCounter = 99;

    int gameResult = -1; 
    

    SetTargetFPS(60);       // Set target frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(currentScreen) 
        {
            //--------------------------LOGO-----------------------------
            case LOGO: 
            {
                framesCounter++;
           
                //QUE PARPADE EL LOGO
                
                if (fading && (framesCounter > 60))
                {
                    alpha += fadeSpeed;
                    
                    if (framesCounter > 170)
                    {
                        fading = !fading;
                    }
                }
                else
                {
                    alpha -= fadeSpeed;
                    
                    if (framesCounter > 280) currentScreen = TITLE;
                }
                
                if (IsKeyPressed(KEY_ENTER)) currentScreen = TITLE;
            } break;
        //--------------------------LOGO-----------------------------
        
            
        //--------------------------TITULO---------------------------    
            case TITLE: 
            {
                if (IsKeyPressed(KEY_ENTER)) currentScreen = GAMEPLAY;
                
                countFrames++;
                
                if (countFrames >= 20)
                {
                    countFrames = 0;
                    drawStart = !drawStart;
                }
            } break;
        //--------------------------TITULO---------------------------    
            

        //---------------------JUEGO FUNCIONANDO---------------------
            case GAMEPLAY:
            { 
                //if (IsKeyPressed(KEY_ENTER)) currentScreen = LOGO;
                
                if (IsKeyPressed(KEY_P)) pause = !pause;
                
                if (!pause)
                {
                    circle.x += circleSpeed.x;
                    circle.y += circleSpeed.y;
                    
                    if (circle.x >= recIa.x)
                    {
                    if (circle.y > (recDerecha.y + recDerecha.height/2)) recDerecha.y += speedEnemy;
                    if (circle.y < (recDerecha.y + recDerecha.height/2)) recDerecha.y -= speedEnemy;
                    }
            
                    if (recDerecha.y <= 0) recDerecha.y = 0;
                    else if ((recDerecha.y + recDerecha.height) >= screenHeight) recDerecha.y = screenHeight - recDerecha.height;
                    
                    if (IsKeyDown(KEY_RIGHT)) recIa.x += 10;
                    if (IsKeyDown(KEY_LEFT)) recIa.x -= 10;

                    if (circle.x > (screenWidth - radio))
                    {
                        enemyLife -= 10;
                        if (enemyLife <= 0) currentScreen = ENDING;
                        
                        circle.x = screenWidth/2;
                        circle.y = screenHeight/2;
                    }

                    if (circle.x < radio)
                    {
                        playerLife -= 10;
                        if (playerLife <= 0) currentScreen = ENDING;
                        
                        circle.x = screenWidth/2;
                        circle.y = screenHeight/2;
                    }

                    if ((circle.y > (screenHeight - radio)) || (circle.y < radio)) circleSpeed.y *= -1;


                    if (IsKeyDown(KEY_UP)) recIzquierda.y -= 5;
                    if (IsKeyDown(KEY_DOWN)) recIzquierda.y += 5;
                    
                    //if (IsKeyDown(KEY_W)) recDerecha.y -= 5;
                    //if (IsKeyDown(KEY_S)) recDerecha.y += 5;
                    
                    if (recDerecha.y < 0 ) recDerecha.y = 0;
                    else if (recDerecha.y > screenHeight - recDerecha.height) recDerecha.y = screenHeight - recDerecha.height;

                    if (recIzquierda.y < 0 ) recIzquierda.y = 0;
                    else if (recIzquierda.y > screenHeight - recIzquierda.height) recIzquierda.y = screenHeight - recIzquierda.height;
                    
                    if (CheckCollisionCircleRec(circle, radio, recDerecha) && circleSpeed.x >= 0)
                    {
                        circleSpeed.x *= -1;
                    }                    
                    else if (CheckCollisionCircleRec(circle, radio, recIzquierda)&& circleSpeed.x <= 0)
                    {
                        circleSpeed.x *= -1;
                    }
                
                    // Reinicia con space
                    if (IsKeyPressed(KEY_SPACE))
                    {
                        circle.x = screenWidth/2;
                        circle.y = screenHeight/2;
                    }
                    
                    framesCounter++;
                    
                    if (framesCounter >= 60)
                    {
                        framesCounter = 0;
                        secondsCounter--;
                        
                        if (secondsCounter <= 0) currentScreen = ENDING;
                    }
                }

            } break;    
        //---------------------JUEGO FUNCIONANDO----------------------
        
        
        //--------------------------ENDING----------------------------        
            case ENDING: 
            {
                if (IsKeyPressed(KEY_ENTER)) 
                {
                    currentScreen = LOGO;
                    playerLife = 100;
                    enemyLife = 200;
                    framesCounter = 0;
                    fading = true;
                    alpha = 0.0f;
                }
               
            } break;
            default: break;
        }
        //--------------------------ENDING----------------------------
        
        
        
//-----------------------------------------------------------------------------------------------------------------------------------------------
// Draw
//------------------------------------------------------------------------------------------------------------------------------------------------
        BeginDrawing();
        
          ClearBackground(RAYWHITE);

        
            switch (currentScreen)
            {
            //--------------------------LOGO-----------------------------
                case LOGO:
                {  
                    DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, Fade(RAYWHITE, alpha));
                } break;
                //--------------------------LOGO-----------------------------
                
                
                //--------------------------TITULO---------------------------
                case TITLE:
                {           
                    DrawText(title, screenWidth/2 - (MeasureText(title, titleSize)/2), 30, titleSize, titleColor);
                    
                    if (drawStart)
                    {
                        DrawText(start, screenWidth/2 - (MeasureText(start,startSize)/2), screenHeight - 200, startSize, startColor);
                    }
                    
                    DrawText(name, screenWidth - 10 - (MeasureText (name, nameSize)), screenHeight - 25, nameSize, nameColor);
                    DrawText(FormatText("%i", countFrames), 10, 10, 25, RAYWHITE);
                } break;
                //--------------------------TITULO---------------------------
                
                
                //---------------------------JUEGO----------------------------      
                case GAMEPLAY:
                {  
                    DrawRectangleRec(recDerecha, colplayer);
                    
                    DrawCircleV(circle, radio, GRAY);
                   
                    DrawRectangleRec(recIzquierda, colenemy);
                    
                    DrawRectangleRec(recIa, GREEN);
                    
                    DrawText(FormatText("%02i", secondsCounter), 380, 10, 30, GRAY);
                    
                    DrawRectangle(25, 10, playerLife, 30, RED);
                    DrawRectangle(550, 10, enemyLife, 30, RED);
                    
                } break;
                //---------------------------JUEGO----------------------------
                
                
                //---------------------------ENDING---------------------------
                case ENDING:
                {    
                    DrawText("ENDING SCREEN", 10, 10, 30, SKYBLUE);
                    
                    if (playerLife <= 0)
                    {
                       DrawText("YOU LOSE", 200, 200, 80, SKYBLUE); 
                    }
                    else
                    {
                      DrawText("YOU WIN", 200, 200, 100, SKYBLUE);  
                    }
                    
                    DrawText("press ENTER for TITLE screen", 230, 350, 20, SKYBLUE);
                } break;
                default: break;
                //---------------------------ENDING---------------------------
            }
        
        EndDrawing();
//-------------------------------------------------------------------------------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}


//---------------------------RESET----------------------------
/*void ResetBall()
{
    circle.x = screenWidth/2;
    circle.y = screenHeight/2;
    circleSpeed.x = GetRandomValue(-20,20);
    circleSpeed.y = GetRandomValue(-20,20);
}
*/
//---------------------------RESET----------------------------